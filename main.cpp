#include <iostream>

#include "inc/SampleClass.h"
#include "inc/SampleClass_2.h"

using namespace std;

int main(int argc, char *argv[])
{
	SampleClass sc(1);
	SampleClass_2 sc_2(2);
	return 0;
}